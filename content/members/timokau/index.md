---
title: Timo Kaufmann
nick: timokau
links:
- GitHub: https://github.com/timokau
- GitLab: https://gitlab.com/timokau
- Mail: mailto:timokau@zoho.com
resources:
- name: avatar
  src: timokau.jpg
---
