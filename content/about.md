---
title: "About Us"
menu: "main"
---

We are the /upb/hack Team - a bunch of students from the university of Paderborn, Germany interested in IT security. We meet regularly and participate in **C**apture **T**he **F**lag challenges, as they are a great way to learn more about security of IT systems.  
Want to join us? Head over to our [Team page](/members/)